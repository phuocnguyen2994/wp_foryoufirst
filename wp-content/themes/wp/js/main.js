$(document).on('ready', function() {
	$(".main_block_img_slide").slick({
		centerMode: true,
		centerPadding: '0',
		slidesToShow: 3,
		autoplay: true,
		autoplaySpeed: 2000,
		responsive: [
		{
			breakpoint: 480,
			settings: {
			  arrows: false,
			  centerMode: true,
			  centerPadding: '0',
			  slidesToShow: 1
			}
		}
		]
	});
    $(".wrap-comment_element").customScrollbar();

    $(".dropdown-select").customScrollbar();

	$('.round').on('click',function(e){
		$(this).addClass('active');
	});

	$('.menu_text ul li').on('click',function(e){
		$(this).addClass('active').siblings().removeClass('active');
	});
  	$('.block_chooseMember_content_form').on('click',function(e){
    	$(this).toggleClass( "open" );
    	$(this).parents('.block_chooseMember_content').find('.block_chooseMember_content_text').toggleClass('close');
  	});
  	$('#list-select li').on('click',function(e){
	    var text = $(this).text();
	    var parent = $(this).parents('.block_chooseMember_content_form');
	    parent.find('.select-button').text(text);
  	});
  	$('.icon_teeth').on('click',function(e){
		$(this).addClass('active').siblings().removeClass('active');
	});
 	 $('button').on('click',function(e){
		$(this).addClass('active').siblings().removeClass('active');
	});
 	$('.main_block_steep_section').matchHeight({ byRow: false });
    $( window ).resize(function() {
      $('.main_block_steep_section').matchHeight({ byRow: false });
    });

    /* Open Menu*/
	$('.navbar-header').click(function(){
    	$('.navbar-collapse ').animate({ right: "0px"
	    }, 200);
	    $('body').css("position", "relative");
	    $('body').animate({
	      right: "224px"
	    }, 200);
  	});
    $('.icon-close').click(function(){
	    $('.navbar-collapse ').animate({ right: "-224px"
	    }, 200);
	    $('body').css("position", "relative");
	    $('body').animate({
	      right	: "0px"
	    }, 200); 
  	});
   
});