<?php  
add_action('wp_enqueue_scripts', 'wp_ldktech_register_assets'  );
function wp_ldktech_register_assets(){

	/*css*/
	wp_enqueue_style('font-awesome-style', 'https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css', array(), '' );
	wp_enqueue_style('scrollbar-style', get_template_directory_uri() . '/css/jquery.custom-scrollbar.css', array(), '');	
	wp_enqueue_style('slick-style', get_template_directory_uri() . '/css/slick.css', array(), '');	
	wp_enqueue_style('slick-theme-style', get_template_directory_uri() . '/css/slick-theme.css', array(), '');		
	wp_enqueue_style('main-style', get_template_directory_uri() . '/css/styles.css', array(), '');
	wp_enqueue_style('responsive-style', get_template_directory_uri() . '/css/responsive.css', array(), '');

	/*js*/

	wp_enqueue_script( 'main-jquery', get_template_directory_uri() . '/js/jquery.min.js', array(), '', true );
	wp_enqueue_script( 'jquery-slick', get_template_directory_uri() . '/js/slick.min.js', array('main-jquery'), '', true );
	wp_enqueue_script( 'jquery-matchHeight', get_template_directory_uri() . '/js/jquery.matchHeight.js', array('main-jquery'), '', true );
	wp_enqueue_script( 'jquery-scrollbar', get_template_directory_uri() . '/js/jquery.custom-scrollbar.min.js', array('main-jquery'), '', true );
	wp_enqueue_script( 'main-scripts', get_template_directory_uri() . '/js/main.js', array('main-jquery'), '', true );
}

add_action( 'after_setup_theme', 'wp_ldktech_setup_element' );
function wp_ldktech_setup_element(){
	register_nav_menus( array(
		'primary_menu' => __( 'Primary Menu'),
		'footer_menu'  => __( 'Footer Menu'),
	) );

	show_admin_bar( false );

	add_theme_support( 'post-thumbnails' );
}
add_action( 'widgets_init', 'wp_web_setup_widgets_init' );
function wp_web_setup_widgets_init() {
	register_sidebar( array(
		'name'          => __( 'Widget footer_1', 'thems_slug' ),
		'id'            => 'footer-1',
		'description'   => __( 'Add widgets here to appear in your sidebar.', 'thems_slug' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<div class="footer_block_title"><h3>',
		'after_title'   => '</h3></div>',
	) );
	register_sidebar( array(
		'name'          => __( 'Widget footer_2', 'thems_slug' ),
		'id'            => 'footer-2',
		'description'   => __( 'Add widgets here to appear in your sidebar.', 'thems_slug' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<div class="footer_top_block_title"><h4>',
		'after_title'   => '</h4></div>',
	) );
	register_sidebar( array(
		'name'          => __( 'Widget footer_3', 'thems_slug' ),
		'id'            => 'footer-3',
		'description'   => __( 'Add widgets here to appear in your sidebar.', 'thems_slug' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<div class="footer_top_block_title"><h4>',
		'after_title'   => '</h4></div>',
	) );
	register_sidebar( array(
		'name'          => __( 'Widget footer_4', 'thems_slug' ),
		'id'            => 'footer-4',
		'description'   => __( 'Add widgets here to appear in your sidebar.', 'thems_slug' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<div class="footer_top_block_title"><h4>',
		'after_title'   => '</h4></div>',
	) );
}