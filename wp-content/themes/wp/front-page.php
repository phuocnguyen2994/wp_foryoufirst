<?php get_header(); ?>
<section>
	<div class="main_block_banner">
		<?php 
			 if (have_posts() ) :
                 while (have_posts() ) :
                   the_post();
		?>
		<div class="thumbnai_banner">
			<?php the_post_thumbnail(); ?>
		</div>
		<div class="block_text">
			<div class="container vcenter">
				<div class="main_block_slide_text">
					<p>ForYouFirst invisible aligner teeth system</p>
					<h1>Unleash the power of a smile</h1>
					<ul>
						<li><a href="">Comfortable</a></li>
						<li><a href="">Affordable</a></li>
						<li><a href="">Versatile</a></li>
						<li><a href="">Metal free tremment</a></li>
						<li><a href="">Quicker</a></li>
					</ul>
				</div>
			</div>
			<div class="overlay vcenter"></div>
		</div>
		<?php
            endwhile;
            wp_reset_postdata();
        endif;
        ?>
		<div class="btn_slider">
			<div class="btn">
				<span></span>
			</div>
		</div>
	</div>
	<div class="main">
		<div class="main_block_aligners">
			<div class="container">
				<div class="main_block_aligners_text">
					<h2>Aligners - ForYouFirst</h2>
					<p>Lorem ipsum dolor sit omen consectetur adipiscing elit. sed do eiusmod tempor incididunt ut labore et dolore magna clique- U! enim ad minim veniom. quis nostmd exercitmion ullomcn laboris nisi ut aliquip ex ca commode conscquot. Duis aute irurc dolor in rcptchcndcrit in voluptute velit esse cillum dolore eu fugicxt nulla pcrictur. Excepteur sint occuecut ctat j proident. sun! in culpo qui ofﬁcio desemnt mollit anim id est laborum. Lorem ipsum dcior sit.  it.</p>
					<a href="#"><span>Can I Have It?</span></a>
				</div>
			</div>
		</div>
		<div class="main_block-block">
			<div class="container">
				<div class="main_block_steep">
					<h2>Three steps to your perfect smile</h2>
					<div class="main_block_steep_item">
						<div class="thumbnai">
							<img src="<?php echo get_template_directory_uri(''); ?>/images/icon1.png" alt="">
						</div>
						<div class="main_block_steep_txt">
							<div>1.
								<p><span>Get evaluated</span></p>
							</div>
						</div>
					</div>
					<div class="main_block_steep_item">
						<div class="thumbnai">
							<img src="<?php echo get_template_directory_uri(); ?>/images/icon2.png" alt="">
						</div>
						<div class="main_block_steep_txt">
							<div>2.
								<p><span>Have your dental<br> impressions done</span></p>	
							</div>
						</div>
					</div>
					<div class="main_block_steep_item">
						<div class="thumbnai">
							<img src="<?php echo get_template_directory_uri(); ?>/images/icon3.png" alt="">
						</div>
						<div class="main_block_steep_txt">
							<div>3.
								<p><span>Smile & More</span></p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="main_block-block orange">
			<div class="container">
				<div class="main_block_person">
					<div class="main_block_text">
						<h2>What People Say</h2>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore e1 dolore magnc clique. Ut enim ad minim veniam, quis nostrud exercitotion ullomco loboris nisi ut oliquip ex ea commodo con equot.</p>
					</div>
					<div class="main_block_img_slide">
						<div class="thumbnai">
							<img src="<?php echo get_template_directory_uri(); ?>/images/img-people1.jpg" alt="">
						</div>
						<div class="thumbnai">
							<img src="<?php echo get_template_directory_uri(); ?>/images/img-people3.jpg" alt="">
						</div>
						<div class="thumbnai">
							<img src="<?php echo get_template_directory_uri(); ?>/images/img-people1.jpg" alt="">
						</div>
						<div class="thumbnai">
							<img src="<?php echo get_template_directory_uri(); ?>/images/img-people1.jpg" alt="">
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section> <!-- end section -->

<?php get_footer(); ?>