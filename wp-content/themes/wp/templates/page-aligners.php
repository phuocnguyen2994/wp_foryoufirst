<?php 
/*Template Name: aligners*/
 get_header(); 
 ?>
<section>
	<div class="main_block_banner">
		<?php 
			 if (have_posts() ) :
                 while (have_posts() ) :
                   the_post();
		?>
		<div class="thumb">
			<?php the_post_thumbnail(); ?>
		</div>
		<div class="block_text">
			<div class="container vcenter">
				<div class="main_block_banner_text">
					<h2><?php the_title(); ?></h2>
					<p><?php the_content(); ?></p>
					<a href="#"><span>Can I Have It?</span></a>
				</div>
			</div>
			<div class="overlay vcenter"></div>
		</div>
		<?php
            endwhile;
            wp_reset_postdata();
        endif;
        ?>
	</div>
	<div class="main">
		<div class="main_block-block">
			<div class="container">
				<div class="main_block_steep">
					<div class="main_block_steep_section">
						<div class="thumbnai">
							<img src="<?php echo get_template_directory_uri(); ?>/images/icon-Confortable.png" alt="">
						</div>
						<div class="main_block_steep_section_txt">
							<h4>Comfortable</h4>
							<p>Clear aligners are completely transparent and discreet, making it the perfect substitute for braces in most cases that requiring teeth straightening. People usually don’t notice them. They’re not magic, however you can see them at close range if you know what to look for.</p>
						</div>
					</div>
					<div class="main_block_steep_section">
						<div class="thumbnai">
							<img src="<?php echo get_template_directory_uri(); ?>/images/icon-Afforadable.png" alt="">
						</div>
						<div class="main_block_steep_section_txt">
							<h4>Affordable</h4>
							<p>Prices at least 4 times cheaper than competitors, without compromising on quality, technology or delivery times.</p>
						</div>
					</div>
					<div class="main_block_steep_section">
						<div class="thumbnai">
							<img src="<?php echo get_template_directory_uri(); ?>/images/Versatile.png" alt="">
						</div>
						<div class="main_block_steep_section_txt">
							<h4>Versatile</h4>
							<p>By being removable, it’s easier for you to brush and floss your teeth very easy, preventing food accumulation in difficult areas to clean like with metal braces. You can use them any time without compromising your confidence.</p>
						</div>
					</div>
					<div class="main_block_steep_section">
						<div class="thumbnai">
							<img src="<?php echo get_template_directory_uri(); ?>/images/Metal-free-tretmans.png" alt="">
						</div>
						<div class="main_block_steep_section_txt">
							<h4>Metal free treatment</h4>
							<p>Made of much softer materials than metal, where you replace each aligner almost every week. Preventing metal allergies and the very uncomfortable experience of the rub of metal on the inside of your mouth.</p>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="main_block_aligners">
			<div class="container">
				<div class="main_block_aligners_text">
					<h2>Aligners - ForYouFirst</h2>
					<p>Lorem ipsum dolor sit omen consectetur adipiscing elit. sed do eiusmod tempor incididunt ut labore et dolore magna clique- U! enim ad minim veniom. quis nostmd exercitmion ullomcn laboris nisi ut aliquip ex ca commode conscquot. Duis aute irurc dolor in rcptchcndcrit in voluptute velit esse cillum dolore eu fugicxt nulla pcrictur. Excepteur sint occuecut ctat j proident. sun! in culpo qui ofﬁcio desemnt mollit anim id est laborum. Lorem ipsum dcior sit.  it.</p>
					<a href="<?php the_permalink(); ?>"><span>Can I Have It?</span></a>
				</div>
			</div>
		</div>
	</div>
</section> <!-- end section -->
<?php get_footer(); ?>