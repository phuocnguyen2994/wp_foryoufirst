<?php 
/*Template Name: smile and more*/
 get_header(); 
 ?>
 <section>
	<div class="main_block_banner">
		<?php 
			 if (have_posts() ) :
                 while (have_posts() ) :
                   the_post();
		?>
		<div class="thumb">
			<?php the_post_thumbnail(); ?>
		</div>
		<div class="block_text">
			<div class="container vcenter">
				<div class="main_block_banner_text">
					<h2>Smile & More</h2>
					<p><?php the_content(); ?></p>
				</div>
			</div>
			<div class="overlay vcenter"></div>
		</div>
		<?php
            endwhile;
            wp_reset_postdata();
        endif;
        ?>
	</div>
	<div class="main">
		<div class="main_block_smile">
			<div class="container">
				<div class="main_block_smile_txt">
					<div>1.
						<p><span class="title">You got your teeth scanned or made and impression at your dentist?</span>
						<span>We Will build personal aligners.</span>
						<span>Send your impression to ForYouFirst.</span></p>
					</div>
				</div>
				<div class="main_block_smile_address">
					<div class="address_info">
						<h4>Address to send impression:</h4>
						<p>ForYouFirst</p>
						<p>Beispielstrasse 1</p>
						<p>1234 Beispielort</p>
					</div>
				</div>
			</div>
		</div>
		<div class="main_block_aligners">
			<div class="container">
				<div class="main_block_profile">
					<div class="main_block_smile_txt">
						<div>2.
							<p><span class="title">Recive your clear aligners at your door or at a ForYouFirst dental provider.</span>
						</div>
					</div>
					<div class="main_block_profile_info">
						<h4>Order my clear aligners</h4>
						<form>
							<input type="text" class="form-control" name="first name" placeholder="First Name" />
							<input type="text" class="form-control" name="last name" placeholder="Last Name" />
							<input type="text" class="form-control" name="first name" placeholder="First Name" />
							<input type="text" class="form-control" name="street" placeholder="Street" />
							<input type="text" class="form-control" name="plz" placeholder="PLZ" />
							<input type="text" class="form-control" name="place" placeholder="Place" />
							<input type="text" class="form-control" name="telephone" placeholder="Telephone" />
							<input type="text" class="form-control" name="email" placeholder="E-Mail">
							<div class="content-check">
								<input type="radio" name="check" id="rd-check" checked="" />
								<label for="rd-check">Please send my clear aligners to my home address</label>
							</div>
							<div class="content-check">
								<input type="radio" name="check" id="rd-uncheck" checked="" />
								<label for="rd-uncheck">Please send my clear aligners to my ForYouFirst dental provider</label>
							</div>
							<textarea class="message" name='comment' id='comment'>Comment</textarea>
						</form>
						<div class="main_block_profile_bnt">
							<button>Order My Clear Aligners</button>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="main_block_smile">
			<div class="container">
				<div class="main_block_smile_txt">
					<div>3.
						<p><span class="title">Use your clear aligners in the correct order, and continue your journey to your perfrct smile.</span></p>
					</div>
				</div>
				<div class="main_block_smile_address">
					<div class="address_info">
						<h4>Any questions?<br/>Contact us on our 24/7 hotline.</h4>
						<span>012 345 67 89</span>
					</div>
				</div>
			</div>
		</div>
	</div>
</section> <!-- end section -->
<?php get_footer(); ?>