<?php 
/*Template Name: howitwork*/
get_header(); ?>
<section>
	<div class="page_howitwork">
		<div class="how">
			<div class="main_block_banner">
				<?php 
					 if (have_posts() ) :
                         while (have_posts() ) :
                           the_post();
				?>
				<div class="thumb">
					<?php the_post_thumbnail(); ?>
				</div>
				<div class="block_text">
					<div class="container vcenter">
						<div class="main_block_banner_text">
							<h2><?php the_title(); ?></h2>
							<p><?php the_content(); ?></p>
							<a href="#"><span>Can I Have It?</span></a>
						</div>
					</div>
					<div class="overlay vcenter"></div>
				</div>
				<?php
		            endwhile;
		            wp_reset_postdata();
		        endif;
		        ?>
			</div>
		</div>
	</div>
	<div class="main">
		<div class="main_block">
			<div class="block_getEvaluated w80_mg0auto">
				<div class="container">
					<div class="page_howitwork_title"><h3>Three steps to your perfect smile</h3></div>
					<div class="page_howitwork_thumbnail vcenter">
						<img src="<?php echo get_template_directory_uri(); ?>/images/icon1.png" alt="">
					</div>
					<div class="page_howitwork_text backgr1 vcenter">
						<div class="page_howitwork_text_title">
							<h5>Get evaluated</h5>
						</div>
						<div class="page_howitwork_text_nav">
							<ul>
								<li><span>Get an initial assessment on our website by sending the requested pictures of your teeth and answering the questionnaire. Designs of the pictures we will request for initial acessement</span></li>
								<li><span>Visit one of our smile pods</span></li>
								<li><span>Visit a for you ﬁrst dental provider. please state your address and we will provide a practitioner close to you for your convenience</span></li>
								<li><span>Need to register on website</span></li>
							</ul>
						</div>
						<div class="page_howitwork_text_button">
							<button><a href="#">Start Now</a></button>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="main_block pink">
			<div class="block_have w80_mg0auto">
				<div class="container">
					<div class="page_howitwork_text backgr2 vcenter">
						<div class="page_howitwork_text_title">
							<h5>Have your dental impressions done</h5>
						</div>
						<div class="page_howitwork_text_nav">
							<ul>
								<li><span>Get your teeth scanned at a smile pad. or we can provide dental scans at your place of work</span></li>
								<li><span>Get your teeth scanned or made an impress ion at your local dentist</span></li>
								<li><span>Leta for you ﬁrst dental provider take your dental impressions. </span></li>
							</ul>
							<div class="page_howitwork_text_nav_span">
								<span>Create a ﬁnder button that will include a close provider when they insert their address.</span>
								<span>Set an appointment.</span>
							</div>
						</div>
						<div class="page_howitwork_text_button">
							<button><a href="#">Start Now</a></button>
						</div>
					</div>
					<div class="page_howitwork_thumbnail vcenter">
						<img src="<?php echo get_template_directory_uri(); ?>/images/icon2.png" alt="">
					</div>
				</div>
			</div>
		</div>
		<div class="main_block">
			<div class="block_smile w80_mg0auto">
				<div class="container">
					<div class="page_howitwork_thumbnail vcenter">
						<img src="<?php echo get_template_directory_uri(); ?>/images/icon3.png" alt="">
					</div>
					<div class="page_howitwork_text backgr3 vcenter">
						<div class="page_howitwork_text_title">
							<h5>Smile & More</h5>
						</div>
						<div class="page_howitwork_text_nav">
							<ul>
								<li><span>Receive your clear oligners at your door or at o for you first dental provider</span></li>
								<li><span>Use your clear oligners in the correct order. and continue the journey to your perfect smile</span></li>
								<li><span>Monitored remotely and 24117 hotline </span></li>
							</ul>
							<div class="page_howitwork_text_nav_span">
								<span>Number hotline</span>
							</div>
						</div>
						<div class="page_howitwork_text_button">
							<button><a href="#">Start Now</a></button>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="wrap-block_letstart">
			<div class="container">
				<div class="block_letstart">
					<h3>Let’s Start</h3>
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididun</p>
					<button><a href="#">Can I Have It?</a></button>
				</div>
			</div>
		</div>
	</div>
</section>
<?php get_footer(); ?>