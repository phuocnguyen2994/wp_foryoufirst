<?php 
/*Template Name: impression*/
 get_header(); 
 ?>
 <section>
	<div class="main_block_banner">
		<?php 
			 if (have_posts() ) :
                 while (have_posts() ) :
                   the_post();
		?>
		<div class="thumb">
			<?php the_post_thumbnail(); ?>
		</div>
		<div class="block_text">
			<div class="container vcenter">
				<div class="main_block_banner_text">
					<h2>Have your dental impressions done</h2>
					<p><?php the_content(); ?></p>
				</div>
			</div>
			<div class="overlay vcenter"></div>
		</div>
		<?php
            endwhile;
            wp_reset_postdata();
        endif;
        ?>
	</div>
	<div class="main">
		<div class="main_block-block">
			<div class="container">
				<div class="main_block_impression_text">
					<h2>Lorem ipsum dolor sit amet</h2>
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
					<h4>Get your teeth scanned or made an impression at your local dentist:</h4>
				</div>
			</div>
		</div>
		<div class="main_block_map">
			<div class="thumbnai_map">
				<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3826.3243723359606!2d107.5795243143028!3d16.459104988641126!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3141a149ddda59d3%3A0x95c5750b90b08a8a!2zNiBMw6ogTOG7o2ksIFbEqW5oIE5pbmgsIFRwLiBIdeG6vywgVGjhu6thIFRoacOqbiBIdeG6vywgVmnhu4d0IE5hbQ!5e0!3m2!1svi!2sin!4v1503109254534" width="100%" height="500" frameborder="0" style="border:0" allowfullscreen></iframe>
			</div>
		</div>
	</div>
</section> <!-- end section -->
<?php get_footer(); ?>