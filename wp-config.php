<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'Foryoufirst');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '+ybDAF1xbTqGr||ykpw=0_94zjCb/|^)H%sxD^36c<nrl36IUWkX<bypmx,zrtA#');
define('SECURE_AUTH_KEY',  'Qp&~k~@hHhkV^()q8aEqfH4!T=,J7{P<vre=0UePFW#<`s}f[;rKLUy}vyxi#jWC');
define('LOGGED_IN_KEY',    'l=(wbFx|4Zf`EiyxMf!xFXG_t3QZs$:mNq[}b$<z^@WR|*--_2R,V(W:G#B-^P+.');
define('NONCE_KEY',        '!WZ.hGXV8%GBN; 2ZXxI4@MS+Or?SENzwi6nWZ~Tql#((pW3r{o`hD%&m,5F6/G-');
define('AUTH_SALT',        'emq5Kon{3G1;pysIfBsg^M|<6RZS +ePJ6@lgN}2mB.mjm[OrJM8g/=FPf5e{J|r');
define('SECURE_AUTH_SALT', '3r sM>vgO6A#goK3:({b<k|~q6%9^NAe6JWbEXgL1v>e|]l+[S6:.)p_9bF]@>7$');
define('LOGGED_IN_SALT',   '(JZ@x95}r=Q]%IZuv>{8!RHY]K$yye:2!t$qvY;E13s4N)S`tx#$9+ Y_rxcto)k');
define('NONCE_SALT',       '|GLj<lpjc!Yk@s@Ud07K_GF v!!$_UiAQsAGRGX`uug(1u#.n!pJwU_|zF9c*0H`');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
