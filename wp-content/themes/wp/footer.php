<footer>
	<div class="footer_top">
		<div class="container">
			<div class="footer_top_block">
				<div class="footer_top_block_logo">
					<a href="<?php echo get_home_url(); ?>"><img src="<?php echo get_template_directory_uri(); ?>/images/logo2.png" alt=""></a>
				</div>
				<div class="footer_top_block_text">
					<?php if ( is_active_sidebar( 'footer-1' ) ) : ?>
							<?php dynamic_sidebar( 'footer-1' ); ?>
					<?php endif; ?>
				</div>
			</div>
			<div class="footer_top_block responsive">
				<?php if ( is_active_sidebar( 'footer-2' ) ) : ?>
						<?php dynamic_sidebar( 'footer-2' ); ?>
				<?php endif; ?>
			</div>
			<div class="footer_top_block">
				<?php if ( is_active_sidebar( 'footer-3' ) ) : ?>
						<?php dynamic_sidebar( 'footer-3' ); ?>
				<?php endif; ?>
			</div>
			<div class="footer_top_block responsive">
				<?php if ( is_active_sidebar( 'footer-4' ) ) : ?>
						<?php dynamic_sidebar( 'footer-4' ); ?>
				<?php endif; ?>
			</div>
		</div>
	</div>
	<div class="footer_bottom">
		<div class="container">
			<div class="footer_bottom_left">
				<p>Copyright For You First 2017</p>
			</div>
			<div class="footer_bottom_right">
				<ul>
					<li><a href="#">Tearms Of User</a></li>       
					<li><a href="#">Disclamer</a></li>
				</ul>
			</div>
		</div>
	</div>
</footer>
	<?php wp_footer(); ?>
</body>
</html>