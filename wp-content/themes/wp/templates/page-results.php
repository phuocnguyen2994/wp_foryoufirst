<?php 
/*Template Name: results*/
 get_header(); 
 ?>
 <section>
	<div class="main_block_banner">
		<?php 
			 if (have_posts() ) :
                 while (have_posts() ) :
                   the_post();
		?>
		<div class="thumb">
			<?php the_post_thumbnail(); ?>
		</div>
		<div class="block_text">
			<div class="container vcenter">
				<div class="main_block_banner_text">
					<h2><?php the_title(); ?></h2>
					<p><?php the_content(); ?></p>
				</div>
			</div>
			<div class="overlay vcenter"></div>
		</div>
		<?php
            endwhile;
            wp_reset_postdata();
        endif;
        ?>
	</div>
	<div class="main">
		<div class="main_block-block ">		
			<div class="main_block_results">
				<div class="container">
					<div class="main_block_results_titile">
						<h2>See Results from our costumers</h2>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris </p>
					</div>
					<div class="main_block_results_layer">
						<div class="layer_item">
							<div class="thumbnai-img">
								<img src="<?php echo get_template_directory_uri(); ?>/images/layer-teeth.png" alt="">
									<div class="thumbnai_teeth">
									</div>
							</div>
						</div>
						<div class="layer_item">
							<div class="thumbnai-img">
								<img src="<?php echo get_template_directory_uri(); ?>/images/layer-teeth.png" alt="">
									<div class="thumbnai_teeth">
									</div>
							</div>
						</div>
						<div class="layer_item">
							<div class="thumbnai-img">
								<img src="<?php echo get_template_directory_uri(); ?>/images/layer-teeth.png" alt="">
									<div class="thumbnai_teeth">
									</div>
							</div>
						</div>
						<div class="layer_item">
							<div class="thumbnai-img">
								<img src="<?php echo get_template_directory_uri(); ?>/images/layer-teeth.png" alt="">
									<div class="thumbnai_teeth">
									</div>
							</div>
						</div>
						<div class="layer_item">
							<div class="thumbnai-img">
								<img src="<?php echo get_template_directory_uri(); ?>/images/layer-teeth.png" alt="">
									<div class="thumbnai_teeth">
									</div>
							</div>
						</div>
						<div class="layer_item">
							<div class="thumbnai-img">
								<img src="<?php echo get_template_directory_uri(); ?>/images/layer-teeth.png" alt="">
									<div class="thumbnai_teeth">
									</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="main_block-block">		
			<div class="main_block_results_txt orange">
				<div class="container-thumb">
					<div class="main_block_results__container">
						<div class="container vcenter">
							<div class="main_block_results_detail vcenter">
								<h2>Your Smile is waiting!!!</h2>
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris </p>
								<a href="#"><span>Can I Have It?</span></a>
							</div>
						</div>
						<div class="overlay vcenter"></div>
					</div>
					<div class="main_block_results_img vcenter">
						<div class="thumbnai">
							<img src="<?php echo get_template_directory_uri(); ?>/images/Fotolia_166926600_S.jpg" alt="">
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="main_block-block">
			<div class="container">
				<div class="main_block_person">
					<div class="main_block_text">
						<h2>What People Say</h2>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore e1 dolore magnc clique. Ut enim ad minim veniam, quis nostrud exercitotion ullomco loboris nisi ut oliquip ex ea commodo con equot.</p>
					</div>
					<div class="main_block_img_slide">
						<div class="thumbnai">
							<img src="<?php echo get_template_directory_uri(); ?>/images/img-people1.jpg" alt="">
						</div>
						<div class="thumbnai">
							<img src="<?php echo get_template_directory_uri(); ?>/images/img-people3.jpg" alt="">
						</div>
						<div class="thumbnai">
							<img src="<?php echo get_template_directory_uri(); ?>/images/img-people1.jpg" alt="">
						</div>
						<div class="thumbnai">
							<img src="<?php echo get_template_directory_uri(); ?>/images/img-people1.jpg" alt="">
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section> <!-- end section -->
<?php get_footer(); ?>