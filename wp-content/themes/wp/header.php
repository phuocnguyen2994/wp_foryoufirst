<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <title><?php bloginfo('name'); ?> <?php if ( is_single() ) { ?> &raquo; Blog Archive <?php } ?> <?php wp_title(); ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
<?php wp_head(); ?>
</head>
<body>
	<header>
		<div class="top_header">
			<div class="container">
				<div class="logo">
					<a href="<?php echo get_home_url(); ?>"><img src="<?php echo get_template_directory_uri(); ?>/images/logo.png" alt=""></a>
				</div>
				<div class="menu">
					<div class="menu_text">
						<?php 
						wp_nav_menu(array(
							'theme_location'  => 'primary_menu', 
		                    'menu_class' =>'nav_menu',
		                    'container' =>''

							) );
					?>	
					</div>					
					<div class="menu_button">
						<button type="submit" class="active">Can I Have It?</button>
						<button type="submit">Member Area</button>
					</div>
				</div>
				<div class="wrapper">
					<div class="navbar-header">
						<i class="fa fa-bars"></i>
					</div>
					<div class="navbar-collapse " id="nav_menu">
				        <div class="icon-close">
				           <i class="fa fa-times"></i>
				    	</div>
						<ul class="collapse_responsive">
							<div class="menu_text">
								<?php 
									wp_nav_menu(array(
										'theme_location'  => 'primary_menu', 
					                    'menu_class' =>'nav_menu',
					                    'container' =>''

										) );
								?>	
							</div>
							<div class="menu_button">
								<button type="submit" class="active">Can I Have It?</button>
								<button type="submit">Member Area</button>
							</div>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</header> <!-- end header -->