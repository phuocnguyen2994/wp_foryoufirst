<?php 
/*Template Name: evaluted*/
 get_header(); 
 ?>
 <section>
	<div class="main_block_banner">
		<?php 
			 if (have_posts() ) :
                 while (have_posts() ) :
                   the_post();
		?>
		<div class="thumb">
			<?php the_post_thumbnail(); ?>
		</div>
		<div class="block_text">
			<div class="container vcenter">
				<div class="main_block_banner_text">
					<h2><?php the_title(); ?></h2>
					<p><?php the_content(); ?></p>
				</div>
			</div>
			<div class="overlay vcenter"></div>
		</div>
		<?php
            endwhile;
            wp_reset_postdata();
        endif;
        ?>
	</div>
	<div class="main">
		<div class="main_block-block">
			<div class="container">
				<div class="main_block_filds">
					<div class="main_block_filds_title">
						<h2>Free Smile Assessment</h2>
						<p>The following questions will help our experienced ortho team decide which treatment is the best for you</p>
					</div>
					<div class="main_block_filds_info">
						<div class="main_block_filds_info_item">
							<p>1. Have you worn braces or clear aligners in the past?</p>
							<button class="active">yes</button>
							<button>no</button>
						</div>
						<div class="main_block_filds_info_item">
							<p>2. Choose which option best describes your biggest concern with your teeth.</p>
							<div class="styled_select">
								<select>
								<option>Choose</option>
								<option>01</option>
								<option>02</option>
								<option>03</option>
								</select>
							</div>
						</div>
						<div class="main_block_filds_info_item">
							<p>3. Which image below best describes your teeth crowding? (Click appropriate image below)</p>
							<div class="icon_teeth active">
								<img src="<?php echo get_template_directory_uri(); ?>/images/icon-filds.png" alt="">
								<i class="check"></i>
							</div>
							<div class="icon_teeth">
								<img src="<?php echo get_template_directory_uri(); ?>/images/icon-filds.png" alt="">
								<i class="check"></i>
							</div>
							<div class="icon_teeth">
								<img src="<?php echo get_template_directory_uri(); ?>/images/icon-filds.png" alt="">
								<i class="check"></i>
							</div>
						</div>
						<div class="main_block_filds_info_item">
							<p>4. Which image below best describes your teeth spacing? (Click appropriate image below)</p>
							<div class="icon_teeth">
								<img src="<?php echo get_template_directory_uri(); ?>/images/icon-filds.png" alt="">
								<i class="check"></i>
							</div>
							<div class="icon_teeth">
								<img src="<?php echo get_template_directory_uri(); ?>/images/icon-filds.png" alt="">
								<i class="check"></i>
							</div>
							<div class="icon_teeth">
								<img src="<?php echo get_template_directory_uri(); ?>/images/icon-filds.png" alt="">
								<i class="check"></i>
							</div>
						</div>
						<div class="main_block_filds_info_item">
							<p>5. Enter your Date of Birth</p>
							<input type="text" class="datepicker" pattern="\d{1,2}/\d{1,2}/\d{4}" name="date" value="" placeholder="yyyy-mm-dd" />
						</div>
						<div class="main_block_filds_info_item">
							<p>6. Choose which option best describes your biggest concern with your teeth.</p>
							<div class="styled_select">
								<select>
								<option>Choose</option>
								<option>01</option>
								<option>02</option>
								<option>03</option>
								</select>
							</div>
						</div>
						<div class="main_block_bnt">
							<button>Complete</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section> <!-- end section -->

<?php get_footer(); ?>